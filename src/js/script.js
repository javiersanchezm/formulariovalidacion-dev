import Swal from 'sweetalert2';

const form = document.getElementById('form');
const nombre = document.getElementById('nombre');
const telefono = document.getElementById('telefono');
const correo = document.getElementById('correo');
const contraseña1 = document.getElementById('contraseña1');
const contraseña2 = document.getElementById('contraseña2');
const fecha = document.getElementById('fecha');

form.addEventListener('submit', e => {
    e.preventDefault();
    revisarInputs()
});

function revisarInputs(){
    const nombreValue = nombre.value.trim();
    const telefonoValue = telefono.value.trim();
    const correoValue = correo.value.trim();
    const contraseñaValue = contraseña1.value.trim();
    const contraseña2Value = contraseña2.value.trim();
    const fechaValue = fecha.value.trim();

    if(nombreValue === ''){
        setErrorFor(nombre, 'No puede dejar el nombre en blanco');
    }
    else{
        setSuccessFor(nombre);
    }

    if(telefonoValue === ''){
        setErrorFor(telefono, 'No puede dejar el telefono en blanco');
    } else if( isTel(telefonoValue) == false){
        setErrorFor(telefono, 'No ingreso un telefono válido (10 dígitos)');
    } else{
        setSuccessFor(telefono);
    }

    if(correoValue === ''){
        setErrorFor(correo, 'No puede dejar el correo en blanco');
    } else if(!isEmail(correoValue)){
        setErrorFor(correo, 'No ingreso un correo válido');
    } else{
        setSuccessFor(correo);
    }

    if(contraseñaValue === ''){
        setErrorFor(contraseña1, 'No puede dejar la contraseña en blanco');
    } else{
        setSuccessFor(contraseña1);
    }

    if(contraseña2Value === ''){
        setErrorFor(contraseña2, 'No puede dejar la contraseña en blanco');
    } else if(contraseña2Value != contraseñaValue){
        setErrorFor(contraseña2, 'Las contraseñas no coinceden');
    } else{
        setSuccessFor(contraseña2);
    }

    let fechaNueva = new Date(fechaValue);
    if(fechaValue === ''){
        setErrorFor(fecha, 'No puede dejar la fecha en blanco');
    } else if( Number(fechaNueva.getFullYear()) > 2004){
        setErrorFor(fecha, "Debes ser mayor de edad");
    }
    else{
        setSuccessFor(fecha);
    }

    let validacionObject = [{
        val: nombre.parentElement.className,
        val: telefono.parentElement.className,
        val: correo.parentElement.className,
        val: contraseña1.parentElement.className,
        val: contraseña2.parentElement.className,
        val: fecha.parentElement.className,
    }];

    const validar = validacionObject.every( valor => valor.val === 'form-control success' );

    if( validar === true){
        alertSuccess();
        resetForm();
    }
}

function setErrorFor(input, message){
    const formControl = input.parentElement;
    const small = formControl.querySelector('small');
    //Agregar la clase error en el form control
    formControl.className = 'form-control error';
    //Agregar mensaje al html que recibe como parámetro
    small.innerText = message;
}

function setSuccessFor(input){
    const formControl = input.parentElement;
    formControl.className = 'form-control success';
}

function isEmail(correo) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(correo);
}

function isTel(telefono) {
	return /^(55|56)[0-9]{8}$/.test(telefono);
}

function alertSuccess(){
    const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.onmouseenter = Swal.stopTimer;
          toast.onmouseleave = Swal.resumeTimer;
        }
      });
      Toast.fire({
        icon: "success",
        title: "Se ha registrado exitosamente"
      });
}

function resetForm(){
    nombre.value = '';
    nombre.parentElement.className = 'form-control';
    telefono.value = '';
    telefono.parentElement.className = 'form-control';
    correo.value = '';
    correo.parentElement.className = 'form-control';
    contraseña1.value = '';
    contraseña1.parentElement.className = 'form-control';
    contraseña2.value = '';
    contraseña2.parentElement.className = 'form-control';
    fecha.value = new Date();
    fecha.parentElement.className = 'form-control';
}